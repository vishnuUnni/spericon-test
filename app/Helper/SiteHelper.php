<?php

namespace App\Helper;

use Auth;
use Modules\User\Entities\Users;

class SiteHelper
{
    public function can($perm,$guard = 'web')
    {
    	$user_id = Auth::guard($guard)->id();
    	$user = Users::find($user_id);
    	$check_list = array();

         //dd($user);
    	foreach ($user->roles()->get() as $key => $value) {
    		$check_list = array_merge($check_list,$value->permissions()->pluck('permissions.slug')->toArray());
    		$check_list = array_unique($check_list);
    	}
    	return in_array($perm, $check_list);
    }




   
    


}

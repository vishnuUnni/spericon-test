@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    @if(session()->has('message'))
                    <div class="m-alert m-alert--outline alert alert-success alert-dismissible  show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">X</button>
                        {{ session()->get('message') }}
                    </div>
                    @endif
	<!-- BEGIN LOGIN -->
		<!-- BEGIN LOGIN FORM -->
		<form class="login-form" name="loginForm" novalidate="" method="post" actiom="{{ url('user/login') }}">
		{{ csrf_field() }}
			<div class="form-group">
				
				<input autocomplete="off" class="form-control" type="email" required="" name="email" id="login_email" placeholder="Email Address">
			</div>
			<div class="form-group">
				 <input autocomplete="off" class="form-control" placeholder="Password" name="password" required="" type="password" id="login_password" >

               @if ($errors->has('password'))
						                <span class="invalid-feedback">
						                    <strong>{{ $errors->first('password') }}</strong>
						                </span>
						            @endif
			</div>

			<div class="form-actions">
				<button class="btn btn-success btn-block submit" type="submit"><span>Sign in</span></button>
			</div>
			<div class="form-actions">
				@if($errors->any())
                                <div class="has-danger" style="color: red">
                                    <strong class="">{{$errors->first()}}</strong>
                                </div>
                                @endif
				<!-- <div class="pull-right forget-password-block">
					<a class="forget-password" href="javascript:;" id="forget-password">Forgot Password?</a>
				</div> -->
			</div>
		</form><!-- END LOGIN FORM -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="col-lg-9" role="main">
    <!-- top tiles -->
    <div class="row ">
          <div class="row">
            <div class=" col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Users  {{$user_count}}</span>
            
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="table-responsive">
                 @if(session()->has('message'))
                    <div class="m-alert m-alert--outline alert alert-success alert-dismissible  show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">X</button>
                        {{ session()->get('message') }}
                    </div>
                    @endif
              <table class="table-bordered table-hover">
                <tbody>
                  <tr>
                  <th>
                    Name
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Image
                  </th>
                  <th>
                    Address
                  </th>
                  <th>
                    Status
                  </th>
                   <th>
                    Actions
                  </th>
                </tr>
                </tbody>
                @foreach($users as $key => $value)
                <tr>
                  <td>
                    {{$value->name}}
                  </td>
                  <td>
                    {{$value->email}}
                  </td>
                  <td>
                    @if($value->image)
                   <img src="{{asset('storage/app')}}/{{$value->image}}" class="img-fluid">
                   @else
                   <img src="{{asset('public/images/user.jpg')}}" class="img-fluid">
                   @endif
                  </td>
                  <td>
                    {{$value->address}}
                  </td>
                  <td>
                    @if($value->status == 1)
                    Active
                    @else
                    Inactive
                    @endif
                  </td>
                   <td>
                    <a href="{{url('/user/changeStatus')}}/{{$value->id}}">Change Status </a>
                    <a href="{{url('/user/delete')}}/{{$value->id}}">Delete</a>
                  </td>
                  </tr>
                @endforeach
              </table>
                {!! $users->render() !!}
            </div>
            </div>
          </div>
          <!-- /top tiles -->
    </div>
</div>
@stop
@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="col-lg-9" role="main">
    <!-- top tiles -->
    <div class="row ">
          <div class="row">
            <div class=" col-xs-6 tile_stats_count">
              <h2>Welcome {{Auth()->user()->name}}</h2>
              @if(Auth()->user()->role_id == 2)
              @if(Auth()->user()->image)
              <img src="{{asset('storage/app')}}/{{Auth()->user()->image}}" class="img-fluid">
              @else
                <img src="{{asset('public/images/user.png')}}" class="img-fluid">
              @endif
              @endif
            </div>
            </div>
          </div>
          <!-- /top tiles -->
    </div>
</div>
@stop
@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="col-lg-9" role="main">
    <!-- top tiles -->
    <div class="row ">
          <div class="row">
            <div class=" col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Permissions</span>
            
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="table-responsive">
                 @if(session()->has('message'))
                    <div class="m-alert m-alert--outline alert alert-success alert-dismissible  show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">X</button>
                        {{ session()->get('message') }}
                    </div>
                    @endif
              <table class="table-bordered table-hover">
                <tbody>
                  <tr>
                  <th>
                    Title
                  </th>
                  <th>
                    Slug
                  </th>
                  <th>
                    Description
                  </th>
                </tr>
                </tbody>
                @foreach($permissions as $key => $value)
                <tr>
                  <td>
                    {{$value->title}}
                  </td>
                  <td>
                    {{$value->slug}}
                  </td>
                  <td>
                    {{$value->description}}
                  </td>
                  </tr>
                @endforeach
              </table>
              
            </div>
            </div>
          </div>
          <!-- /top tiles -->
    </div>
</div>
@stop
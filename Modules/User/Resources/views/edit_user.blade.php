@extends('layouts.app')
@section('content')

<!-- page content -->
<div class="col-lg-9" role="main">
    <!-- top tiles -->
    <div class="row ">
          <div class="row">
            <div class=" col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Edit Details</span>
            
            </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="table-responsive">
                 @if(session()->has('message'))
                    <div class="m-alert m-alert--outline alert alert-success alert-dismissible  show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">X</button>
                        {{ session()->get('message') }}
                    </div>
                    @endif
                    @if($errors->any())
                                <div class="has-danger" style="color: red">
                                    <strong class="">{{$errors->first()}}</strong>
                                </div>
                                @endif
            </div>
            <form method="post" enctype="multipart/form-data" action="{{URL('/user/update')}}">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{$user->id}}">
            <div class="row">
              <div class="col-lg-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" value="{{$user->name}}" class="form-control">
                </div>
              </div>
               <div class="col-lg-6">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" value="{{$user->email}}" class="form-control">
                </div>
              </div>
               <div class="col-lg-6">
                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" name="address" value="{{$user->address}}" class="form-control">
                </div>
              </div>
               <div class="col-lg-6">
                  <div class="custom-file">
                    <input type="file" name="image"  class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
              </div>
               <div class="col-lg-6">
                  <div class="form-group">
                    <label>Pasword</label>
                    <input type="password" name="password"  class="form-control">
                </div>
              </div>
               <div class="col-lg-12">
              <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update</button>  
                </div>
              </div>
             
            </div>
             </form>
            </div>
          </div>
          <!-- /top tiles -->
    </div>
</div>
@stop

<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB, Config;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Authenticatable
{
    protected $table = 'users';
    protected $primaryKey = "id";
    use SoftDeletes;
    
    public function roles() {
        return $this->belongsTo("Modules\User\Entities\Role","role_id");
    }
}

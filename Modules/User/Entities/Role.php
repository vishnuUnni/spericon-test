<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    protected $table = 'roles';
     use SoftDeletes;

    public function permissions() {
        return $this->belongsToMany("Modules\User\Entities\Permission","role_permissions","role_id","perm_id");
    }
}

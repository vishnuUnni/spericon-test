<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    protected $table = 'permissions';
     use SoftDeletes;

    public function roles() {
        return $this->belongsToMany("Modules\User\Entities\Role","role_permissions","perm_id","role_id");
    }
}

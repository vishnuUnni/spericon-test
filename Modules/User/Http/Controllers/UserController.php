<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Crypt;
use Modules\User\Entities\Users;
use Modules\User\Entities\Permission;
use Mail;
use Auth;
use URL;
use Hash;
class UserController extends Controller
{
 use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/users';

    /**
     * Create a new controller instance.
     *
     * @return void
     */



    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function login(Request $request)
    {
        //dd($request->all());
         $this->validate($request, [
            'email' => 'required|email', 
            'password' => 'required',
        ]);
        $user = Users::where('email',$request->email)->first();
        //dd($user);
        if($user == null) {
            return redirect('/user/login')->withErrors(['No such user exist.']);
        } 
        else
        {
            if($user->status == 0)
            {
                return redirect('/user/login')->withErrors(['You are not active yet']);
            }
            $credentials = $request->only('email', 'password');
            if (Auth::guard('web')->attempt($credentials)) {

                return redirect('/user');
            }
            else
            {
                return redirect('/user/login')->withErrors(['Provide a valid username or password']);
            }
        }  
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function user_list()
    {

        $user_count=Users::where('role_id',2)->count();
        $users=Users::where('role_id',2)->paginate(10);
        return view('user::user_list')->with(compact('user_count','users'));
    }
    public function dashboard()
    {
         return view('user::dashboard');
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 
            'email' => 'required|email|unique:users',
            'address' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);
        $user = new Users;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->role_id=2;
        $user->address=$request->address;
        $user->password=Hash::make($request->password);
        $user->status=0;
        $user->save();
        return redirect()->back()->with('message','Suceessfully Registered Please wait for Admin Confirmation');

     }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function showLoginForm()
    {
         if(Auth::guard('web')->check())
            return redirect('/user');
        return view('user::login');
    }
    public function showRegisterForm()
    {
         
        return view('user::register');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function changeStatus($id)
    {
        $user=Users::where('id',$id)->first();
        if($user->status ==0)
        {
            $user->status =1;
        }
        else
        {
            $user->status=0;
        }
        $user->save();
        return redirect()->back()->with('message','Status Changed Suceessfully');
    }
    public function editProfile()
    {
        $user=Auth()->user();
        return view('user::edit_user')->with(compact('user'));
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request)
    {
         $this->validate($request, [
            'name' => 'required', 
            'email' => 'required|email',
            'address' => 'required',           
        ]);
         $user=Users::where('id',$request->id)->first();
         $user->name=$request->name;
         $user->email=$request->email;
         $user->address=$request->address;
         if($request->file('image'))
         {
         $user->image = $request->file('image')->store('users');
        }
         if($request->password)
         {
            $user->password = Hash::make($request->password);
         }
         $user->save();
        return redirect()->back()->with('message','Profile Updated Suceessfully');
        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $user=Users::find($id);
        $user->delete();
        return redirect()->back()->with('message','User Deleted Suceessfully');
    }
     public function forbidden()
    {
            return view('user::forbidden');
    }
    public function permissions()
    {
        $permissions=Permission::where('status',1)->get();
        return view('user::permissions')->with(compact('permissions'));
    }
}

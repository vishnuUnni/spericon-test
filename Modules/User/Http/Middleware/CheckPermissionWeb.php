<?php

namespace Modules\User\Http\Middleware;

use Closure;
use SiteHelper;
use Auth;

class   CheckPermissionWeb
{
    /**
     * For checking user permission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next ,$permission = null,$guard="web")
    {
        if (!SiteHelper::can($permission) ) {

             if ($request->ajax()) { 
                return response([
                    'error' => 'Forbidden',
                    'error_description' => 'Permission denied.',
                    'data' => [],
                ], 403);
            } else {

                return redirect('/user/forbidden');
            }
        }

        return $next($request);
    }
}

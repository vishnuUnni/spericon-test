<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user')->group(function() {
       Route::get('login', 'UserController@showLoginForm')->name('login');
       Route::get('register', 'UserController@showRegisterForm')->name('register');
	Route::post('login ', 'UserController@login');
	Route::post('register ', 'UserController@store');
	Route::get('logout', 'UserController@logout')->name('user.logout');

	Route::get('forbidden', 'UserController@forbidden');

	Route::group(['middleware' => 'admin_auth:web'], function(){
		Route::get('/changeStatus/{id}', ['middleware' => 'permission:changeStatus', 'uses' => 'UserController@changeStatus']);
		Route::get('/delete/{id}', ['middleware' => 'permission:deleteUser', 'uses' => 'UserController@destroy']);
		Route::get('/', ['middleware' => 'permission:dashboard', 'uses' => 'UserController@dashboard']);
		Route::get('/users', ['middleware' => 'permission:users', 'uses' => 'UserController@user_list']);
		Route::get('/editProfile', ['middleware' => 'permission:editProfile', 'uses' => 'UserController@editProfile']);
		Route::post('/update ', ['middleware' => 'permission:editProfile', 'uses' => 'UserController@update']);
		Route::get('/permissions', ['middleware' => 'permission:seePermissions', 'uses' => 'UserController@permissions']);
	});
});
